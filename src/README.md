
Your name: 	Austin Hutchison

 A list of all the files in your submission and what each does.
	
	~src/info/cpu_info.{h,cpp}		// Gets and holds cpu related info
	~src/info/load_average_info.{h,cpp}	// Gets and holds average load info
	~src/info/memory_info.{h,cpp}		// Gets and holds memory related info
	~src/info/process_info.{h,cpp}		// Gets info on each process and their threads
	~src/info/system_info.{h,cpp}		// Collects info from above files and loads it into one struct

	~src/utils/README.md			// Project Info
	~src/utils/flags.{h,cpp}			// This processes the flags and gives mytop something useful
						//	(although probably poorly implemented. I was on a time table 
						//	so I stuck to what I knew as much as possible)
	~src/utils/formatting.{h,cpp}		// Formates memory or time into Human readable data
	~src/utils/stats.{h,cpp}			// This does all the calculations needed for mytop, 
						//	and puts all the displayable data in a single struct

	~src/main.cpp				// This is the heart of the progam. It handles displaying the data

 Any unusual/interesting features in your programs.
	
	~ I added it so you could press 'h' during the mytop execution to also see the help message
	~ Sorting by memory ignores units. I know this is a 'bug', but I'm trying to get this done with only one skip day

 Approximate number of hours you spent on the project.

	~ An ungodly amount of time (In all seriousness, I didn't keep track, Probably 40+)

 The ideal terminal size at which to run your top implementation (if it’s not a reasonable size,
then don’t expect a perfect grade

	~My monitor is 1920x1080. mytop runs fine(2/3's of the window) in the window with a realtively large zoom on the display (16-point font)
		~So I feel like it could run in most full screen windows.
		~ Some of the command lines are really long though and cause viewing issues. I wasn't sure how to handle that though.

