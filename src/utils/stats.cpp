// This implements stats.h

#include "stats.h"
#include "formatting.h"
#include <string>
#include <vector>
#include "../info/cpu_info.h"
#include "../info/load_average_info.h"
#include "../info/memory_info.h"
#include "../info/process_info.h"
#include "../info/system_info.h"
#include <unistd.h>
#include <sys/times.h>

using namespace std;


// A function that fills topStats up
topStats get_topStats(SystemInfo &oldSys, SystemInfo &sys, const string &sortMethod){	// Old and new for stats and utilization calcs
	
	// Declare Variables
	topStats tops;

	// Get Uptime
	double upTime = sys.uptime;
	unsigned long long upTimeTicks= upTime * 100;
	humanRead upTimeHuman = humanTime(upTimeTicks);
	
	// Get cpu Delta's and utilizations
	vector<CpuInfo> cpuDelta;
	vector<utilize> cpuUtiliz;
	for (unsigned i=0; i < sys.cpus.size(); i++){
		cpuDelta.push_back(sys.cpus[i] - oldSys.cpus[i]);
		cpuUtiliz.push_back(getUtilization(cpuDelta[i]));
	}

	// Get memory
	humanRead totalMem = humanMem(sys.memory_info.total_memory);
	humanRead usedMem = humanMem(sys.memory_info.total_memory - sys.memory_info.free_memory);
	humanRead availMem = humanMem(sys.memory_info.free_memory);	

	// Get and Sort Processes
	vector<processStats> sortedProc;
 	double dUpTime = sys.uptime - oldSys.uptime;
		// Get Processes
	for (unsigned i=0; i < sys.processes.size(); i++) {
		for (unsigned j=0; j < oldSys.processes.size(); j++) {
			if (sys.processes[i].pid == oldSys.processes[j].pid) {
				sortedProc.push_back(getProcessStat(oldSys.processes[j], sys.processes[i], dUpTime));	
			}
		} 
	}
		// Sort Processes
	sortTheProcs(sortedProc, sortMethod);
	

	// Fill up tops
	tops.sysUpTime = upTimeHuman.humanTime;
	tops.sysUpTimeUnit = upTimeHuman.timeUnit;
	
	tops.loadAvgOne = sys.load_average.one_min;
	tops.loadAvgFive = sys.load_average.five_mins;
	tops.loadAvgFifteen = sys.load_average.fifteen_mins;
	
	tops.cpuUtilize = cpuUtiliz;
	
	tops.totalProcExist = sys.num_processes;
	tops.totalProcRun = sys.num_running;
	tops.totalThreads = sys.num_threads;
	
	tops.totalMemory = totalMem.humanBytes;
	tops.totalMemUnit = totalMem.memUnit;
	
	tops.memInUse = usedMem.humanBytes;
	tops.memInUseUnit = usedMem.memUnit;

	tops.memAvail = availMem.humanBytes;
	tops.memAvailUnit = availMem.memUnit;
	
	tops.sortProc = sortedProc;

	// Return
	return tops;


}
// This fills up the utilize function
utilize getUtilization(const CpuInfo &cpuDelta){

	
	// Declare variables
	utilize cpuUtilize;	// Return value
	unsigned long long userTime = cpuDelta.user_time + cpuDelta.nice_time;
	unsigned long long kernelTime = cpuDelta.total_system_time() + cpuDelta.total_virtual_time();
	
	// Fill up cpuUtilize
	cpuUtilize.userCent = (userTime * 100.00) / cpuDelta.total_time();		// User percentage
	cpuUtilize.kernelCent = (kernelTime * 100.00) / cpuDelta.total_time();		// Kernel Percentage
	cpuUtilize.idleCent = (cpuDelta.total_idle_time() * 100.00) / cpuDelta.total_time();	// Idle percentage

	// Return
	return cpuUtilize;
}



// Fills up the processStats and updates the passed processinfo with cpu percent
processStats getProcessStat(ProcessInfo &oldProc, ProcessInfo &newProc, const double &dTime){

	// Declare variables
	processStats procStats; 	// Return value
	

	// Get resident size
	unsigned long long rssByte = newProc.rss * _SC_PAGE_SIZE;
	humanRead rssMem = humanMem(rssByte);
	
	// Get delta time between process for CPU percent
	unsigned long long oldTime = oldProc.utime + oldProc.stime + oldProc.cutime + oldProc.cstime + oldProc.guest_time + oldProc.cguest_time;
	unsigned long long newTime = newProc.utime + newProc.stime + newProc.cutime + newProc.cstime + oldProc.guest_time + oldProc.cguest_time;
	double deltaTime = (newTime - oldTime)/100.00;
	newProc.cpu_percent = (deltaTime * 100.00) / dTime;
	
	// Time spent on cpu
	humanRead totProcTime = humanTime(newTime);

	// Fill up procStats;
	procStats.pid = newProc.pid;
	procStats.resMem = rssMem.humanBytes;
	procStats.resMemUnit = rssMem.memUnit;
	procStats.currentState = newProc.state;
	procStats.cpuPercent = newProc.cpu_percent;
	procStats.procTime = totProcTime.humanTime;
	procStats.procTimeUnit = totProcTime.timeUnit;
	procStats.theCmdLine = newProc.command_line;

	// Return
	return procStats;
}

//swaps i and i+1
void swapProcs(vector<processStats> &theProcs, unsigned i) {
	processStats bigCent = theProcs[i+1];
	theProcs[i+1] = theProcs[i];
	theProcs[i] = bigCent;

}
void sortTheProcs(vector<processStats> &theProcs, const string &sortMethod) {

	// Did someone say SORTING PARTY?  HOO-RAY!!!
	for (unsigned i=0; i<theProcs.size(); i++) {
		if (i != ( theProcs.size() - 1)){
			if ( sortMethod == "CPU" && (theProcs[i].cpuPercent <  theProcs[i+1].cpuPercent)) {
				swapProcs(theProcs, i);
				i = -1;
			} else if ( sortMethod == "PID" && (theProcs[i].pid <  theProcs[i+1].pid)) {
				swapProcs(theProcs, i);
				i = -1;
			} else if ( sortMethod == "MEM" && (theProcs[i].resMem <  theProcs[i+1].resMem)) {
				swapProcs(theProcs, i);
				i = -1;
			} else if ( sortMethod == "TIME" && (theProcs[i].procTime <  theProcs[i+1].procTime)) {
				swapProcs(theProcs, i);
				i = -1;
			}
		}
	}	
}

