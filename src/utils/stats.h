// This is a header file that outlines statistics 
#pragma once
#include <string>
#include <vector>
#include "../info/cpu_info.h"
#include "../info/load_average_info.h"
#include "../info/memory_info.h"
#include "../info/process_info.h"
#include "../info/system_info.h"

using namespace std;

// Used to hold utilizing percentages of processors
struct utilize {

	double userCent;	// Percent of time spent in user mode

	double kernelCent;	// Percen of time spent in kernel mode

	double idleCent;	// Percent of time spent idiling
};

// This fills up the utilize function
utilize getUtilization(const CpuInfo &cpuDelta);


// Used to hold stats about processes 
struct processStats {
	
	int pid;		// Process ID (PID)

	double resMem;		// Process Resident memory size
	string resMemUnit;	// Unit for resMem

	char currentState;	// Process Current state (single-letter abbreviation)
	
	double cpuPercent;	// % of CPU currently being used

	double procTime;	// Total amount of time spent being executed by a processor
	string procTimeUnit;	// Unit for procTime

	string theCmdLine;	// The name of the binary (or the cmdline) that was executed

};

// Fills up the processStats and updates the passed processinfo with cpu percent
processStats getProcessStat(ProcessInfo &oldProc, ProcessInfo &newProc, const double &dTime);	// Old and new for process stats and process cpu utilization

void sortTheProcs(vector<processStats> &theProcs, const string &sortMethod);

// The topStats struct contains all the statistics used by top
struct topStats {

	double sysUpTime;		//System uptime: how long the computer has been on, human readable
	string sysUpTimeUnit;		//System uptime unit (Years, Days, Hours, Minutes, or Seconds)
	
	double loadAvgOne;		// The average load of the system over the last one minute.
	double loadAvgFive;		// The average load of the system over the last five minutes.
	double loadAvgFifteen;		// The average load of the system over the last fifteen minutes.
	
	vector<utilize> cpuUtilize;	// User, Kernel, and idle percentages for each processor in order. Overall utilization is first entry.

	unsigned long totalProcExist;	// Total number of processes that currently exist on the system
	unsigned long totalProcRun;	// Total number of processes that are currently running
	unsigned long totalThreads;	// Total number of threads (both user-land and kernel-level)

	double totalMemory;		// The amount of memory that is installed on the machine
	string totalMemUnit;		// Unit for totalMemory

	double memInUse;		// The amount of memory that is currently in use
	string memInUseUnit;		// Unit for memInUse;

	double memAvail;		// The amount of memory that is currently available
	string memAvailUnit;		// Unit for memAvailUnit

	vector<processStats> sortProc;	// All Processes,and their stats, that have are sorted accordingly

};

// A function that fills topStats up
topStats get_topStats(SystemInfo &oldSys, SystemInfo &sys, const string &sortMethod);	// Old and new for stats and utilization calcs


