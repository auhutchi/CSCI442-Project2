/*
	 Code to help with formatting numbers in readable formats,
	 e.g. human-readably bytes (B, KiB, MiB, GiB, etc)
	 and time strings (seconds, minutes, hours, days, etc).
*/

#pragma once
#include <string>
using namespace std;

// a struct that reads in a byte value and a memory value then converts them to a readable value and a string to indicate units
struct humanRead {
	
	string memUnit;			// Stores the unit of the humanBytes value
	
	double humanBytes;		// Stores a memory size of value less than 1024 since the unit changes every magnitude of 2^10

	string timeUnit;		// Stores the unit of the humanTime Value

	double humanTime;		// Stores it into a readable time
};


// Turns inBytes into something readable
humanRead humanMem(const unsigned long long &inBytes);


// Turns inTime into something readable
humanRead humanTime(const unsigned long long &inTime);
