// Tests the formatting functions

#include "formatting.h"
#include "gtest/gtest.h"

using namespace std;

TEST(GetFormatting, MemoryVala) {
	unsigned long long byter = 524299;	// Just over 512 MiB
	humanRead human = humanMem(byter);
	EXPECT_NEAR(512.0107, human.humanBytes, 0.01);
}

TEST(GetFormatting, MemoryUnita) {
	unsigned long long byter = 524299;	// Just over 512 MiB
	humanRead human = humanMem(byter);
	EXPECT_EQ("KiB", human.memUnit);
}

TEST(GetFormatting, MemoryValb) {
	unsigned long long byter = 657398431744;	// 612.25 GiB
	humanRead human = humanMem(byter);
	EXPECT_NEAR(612.25, human.humanBytes, 0.01);
}

TEST(GetFormatting, MemoryUnitb) {
	unsigned long long byter = 657398431744;	// 612.25 GiB
	humanRead human = humanMem(byter);
	EXPECT_EQ("GiB", human.memUnit);
}

TEST(GetFormatting, TimeVala) {
	unsigned long long time = 78700;	// 13.117 minutes
	humanRead human = humanTime(time);
	EXPECT_NEAR(13.116, human.humanTime, 0.01);
}

TEST(GetFormatting, TimeUnita) {
	unsigned long long time = 78700;	// 13.117 minutes
	humanRead human = humanTime(time);
	EXPECT_EQ("Minutes", human.timeUnit);
}

TEST(GetFormatting, TimeValb) {
	unsigned long long time = 56784251346;	// 17.9938 Years
	humanRead human = humanTime(time);
	EXPECT_NEAR(17.9938, human.humanTime, 0.01);
}

TEST(GetFormatting, TimeUnitb) {
	unsigned long long time = 56784251346;	// 17.9938 Years
	humanRead human = humanTime(time);
	EXPECT_EQ("Years", human.timeUnit);
}
