#include <vector>
#include <getopt.h>
#include <iostream>
#include <cstdlib>
#include "flags.h"
#include <stdlib.h>
#include <string>
using namespace std;

vector<string> getFlag(int argc, char**argv) {

  vector<string> inputArgs(3,"0");

  // Alot of copy an past

   // An array of long-form options.
  static struct option long_options[] = {
    {"delay",	required_argument, NULL, 'd'},
    {"sort-key",required_argument, NULL, 's'},
    {"help", 	no_argument, NULL, 'h'},
   
    // Terminate the long_options array with an object containing all zeroes.
    {0, 0, 0, 0}
  };

  // getopt_long parses one argument at a time. Loop until it tells us that it's
  // all done (returns -1).
  while (true) {
    // getopt_long stores the latest option index here,you can get the flag's
    // long-form name by using something like long_options[option_index].name
    int option_index = 0;

    // Process the next command-line flag. the return value here is the
    // character or integer specified by the short / long options.
    int flag_char = getopt_long(
        argc,           // The total number of arguments passed to the binary
        argv,           // The arguments passed to the binary
        "d:s:h",     // Short-form flag options
        long_options,   // Long-form flag options
        &option_index); // The index of the latest long-form flag

    // Detect the end of the options.
    if (flag_char == -1) {
      break;
    }

    switch (flag_char) {
    case 0:
      cout << "Saw --" << long_options[option_index].name << " flag" << endl;
      break;

    case 'd':
      		inputArgs[0] = optarg;
      break;

    case 's':
      		inputArgs[1] = optarg;
      break;

    case 'h':
      		inputArgs[2] = "1";
      break;

     case '?':
      // This represents an error case, but getopt_long already printed an error
      // message for us.
      break;

    default:
      // This would only happen if a flag hasn't been handled, or if you're not
      // detecting -1 (no more flags) correctly.
      exit(EXIT_FAILURE);
    }
  }


  // Print any other command line arguments that were not recognized as flags.
  if (optind < argc) {
    cout << "Non-option ARGV-elements: ";

    for (int i = optind; i < argc; i++) {
      cout << argv[i] << " ";
    }

    cout << endl;
  }

  return inputArgs;
}
