// Containts the implementation for formatting.h

#include "formatting.h"
#include <string>

using namespace std;

// Turn inBytes into something readable
humanRead humanMem(const unsigned long long &inBytes) {
	
	// Variable declarations
	humanRead readable;
	unsigned power = 0;	// used for units
	string unit = "B"; 	// The current unit of bytes
	double outBytes = inBytes;
	
	// Convert into something a readable unit size
	while(outBytes > 1025) {
		outBytes = outBytes/1024.00;
		power = power + 10;
	}
	
	// Determine the unit
	if (power == 10) {
		unit = "KiB";
	} else if (power == 20) {
		unit = "MiB";
	} else if (power == 30) {
		unit = "GiB";
	} else if (power == 40) {
		unit = "TiB";
	} else if (power != 0) {
		unit = "Err";
	}

	// Save values
	readable.memUnit = unit;
	readable.humanBytes = outBytes;

	// Return 
	return readable;

}

// Turn inTime into something readable
humanRead humanTime(const unsigned long long &inTime) {
	
	// Declare Variables
	humanRead readable;
	string unit = "Seconds";
	double time = inTime/100.00;		// Assumes inTime units  is == to 1/100 seconds
	
	// Convert into readable
	if (time > 120) {
		time = time / 60.00;	// Convert into minutes
		unit = "Minutes";
		if (time > 120) {
			time = time / 60.00;	// Convert into hours
			unit = "Hours";
			if (time > 48) {
				time = time / 24.00;	// Convert into days
				unit = "Days";
				if (time > 365.25) {
					time = time / 365.25;	//Convert into years
					unit = "Years";
				}
				if (time > 100) {
					unit = "Err";
				}
			}
		}
	}

	// Save values
	readable.timeUnit = unit;
	readable.humanTime = time;

	// Return
	return readable;
}

