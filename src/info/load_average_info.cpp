#include "load_average_info.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>

using namespace std;

// This function is strongly similar to the code Josh posted on piazza.
LoadAverageInfo get_load_average() {
 
	// Open file
	ifstream loadAvgFile(PROC_ROOT "/loadavg");

	// Check for error, If error,  print error and exit
	if (!loadAvgFile) {
		cerr << "Unable to read from /proc/loadavg" << endl;
		exit(EXIT_FAILURE);
	}

	// Create a load average variable
	LoadAverageInfo loadAverage;

	// Read the values. 
	loadAvgFile
		    >> loadAverage.one_min
		    >> loadAverage.five_mins
		    >> loadAverage.fifteen_mins;
	return loadAverage;
}
