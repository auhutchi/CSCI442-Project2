#include "memory_info.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdlib.h>

using namespace std;


MemoryInfo get_memory_info() {
  	
	//Open File.
	ifstream memFile(PROC_ROOT "/meminfo");

	// Check for error, If error print and exit
	if (!memFile) {
		cerr << "Unable to read from /proc/meminfo" << endl;
		exit(EXIT_FAILURE);
	}

	// Declare a memory info variable
	MemoryInfo memInfo;

	// Search through file
	int fishesAte = 0;
	while (fishesAte < 6) {
	
		string sharkAte;	// Eats the stuff that comes out of the stream.
		memFile >> sharkAte;

		// Check to see if the shark ate something good.
		if (sharkAte == "MemTotal:") {
			memFile >> memInfo.total_memory;
		} else if (sharkAte == "MemFree:") {
			memFile >> memInfo.free_memory;
		} else if (sharkAte == "Buffers:") {
			memFile >> memInfo.buffers_memory;
		} else if (sharkAte == "Cached:") {
			memFile >> memInfo.cached_memory;
		} else if (sharkAte == "SwapTotal:") {
			memFile >> memInfo.total_swap;
		} else if (sharkAte == "SwapFree:") {
			memFile >> memInfo.free_swap;
		} else {
			continue;
		}
		
		// If the shark ate something good, increment while counter
		fishesAte++;
	}
	
	
  return memInfo;
}
