#include "process_info.h"
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;


ProcessInfo get_process(int pid, const char* basedir) {
  	
	// create a process variable
	ProcessInfo process;
	
	// Get info from /proc/pid/statm
		// Open stam file
		stringstream statms;
		statms << basedir << "/" << pid << "/statm";
		ifstream statm(statms.str().c_str());

		// Check for errors opening file
		if (!statm) {
			cerr << "Unable to read from /[basedir]/[pid]/statm" << endl;
			//exit(EXIT_FAILURE);
			return process;
		}

		// Eat data out of stam
		statm	>> process.size
			>> process.resident
			>> process.share
			>> process.trs
			>> process.lrs
			>> process.drs
			>> process.dt;

	// Get info from basedir/pid/stat
		// Open stat file
		stringstream stats;
		stats << basedir << "/" << pid << "/stat";
		ifstream stat(stats.str().c_str());

		// Check for erros opening file
		if (!stat) {
			cerr << "Unable to read from /[basedir]/[pid]/stat" << endl;
			//exit(EXIT_FAILURE);
			return process;
		}
		// The stat data eating contest. 
		stat	>> process.pid
			>> process.comm
			>> process.state
			>> process.ppid
			>> process.pgrp
			>> process.session
			>> process.tty_nr
			>> process.tpgid
			>> process.flags
			>> process.minflt
			>> process.cminflt
			>> process.majflt
			>> process.cmajflt
			>> process.utime
			>> process.stime
			>> process.cutime
			>> process.cstime
			>> process.priority
			>> process.nice
			>> process.num_threads
			>> process.itrealvalue
			>> process.starttime
			>> process.vsize
			>> process.rss
			>> process.rsslim
			>> process.startcode
			>> process.endcode
			>> process.startstack
			>> process.kstkesp
			>> process.kstkeip
			>> process.signal
			>> process.blocked
			>> process.sigignore
			>> process.sigcatch
			>> process.wchan
			>> process.nswap
			>> process.cnswap
			>> process.exit_signal
			>> process.processor
			>> process.rt_priority
			>> process.policy
			>> process.delayacct_blkio_ticks
			>> process.guest_time
			>> process.cguest_time;

	// get data from basedir/pid/cmdline
		// Open stam file
		stringstream cmdlines;
		cmdlines << basedir << "/" << pid << "/cmdline";
		ifstream cmdline(cmdlines.str().c_str());

		// Check for errors opening file
		if (!cmdline) {
			cerr << "Unable to read from /[basedir]/[pid]/cmdline" << endl;
			//exit(EXIT_FAILURE);
			return process;
		}
		

		// Eat data from cmdline
		cmdline >> process.command_line;

		// Annoying
		replace(process.command_line.begin(), process.command_line.end(), '\0', ' ');
		// Deal with the nasty data
//grr		size_t pos =  0;
//		while (pos != string::npos){
//			pos = process.command_line.find('\0', 0);
//			process.command_line.replace(pos, 1, " ");
//		}
		if (process.command_line.empty()){
			process.command_line = process.comm;
			process.command_line.erase(process.command_line.begin());
			process.command_line.erase(process.command_line.end() - 1);

		} else {
			process.command_line.erase(process.command_line.end() - 1);
		}

	// Set the tgid to pid. This will be overwritten if thread while making threads vector
		process.tgid = process.pid;

	// Create the threads vector
		stringstream threadss;
		threadss << basedir << "/" << pid << "/task";
		process.threads = get_all_processes(threadss.str().c_str());
		for (unsigned int i = 0; i < process.threads.size(); i++) {
			process.threads[i].tgid = process.pid;
		}
	
	// Return the process
	return process;
}


vector<ProcessInfo> get_all_processes(const char* basedir) {
	// Create a vector to store the processes on
	vector<ProcessInfo> allProcesses;

	// Create a dirent to read directories in basedir
	dirent* dirrecords;

	// Open the base director basedir
	DIR *baseDirectory;
	baseDirectory = opendir(basedir);
	if (baseDirectory == NULL) {		// Check for erros
	//	cout << basedir << endl;
	//	perror("opendir error");
		return allProcesses;	
	}
	
	
	// Create a process for every process/directory in basedir
	while((dirrecords = readdir(baseDirectory)) != NULL) {
		// Get the pid of the process
		int pid = atoi(dirrecords->d_name);
		
		// If d_name couldn't be converted, skip entry
		if (pid <= 0) {
			continue;
		}

		// Create the process
		ProcessInfo process = get_process(pid, basedir);
		
		// Push the process onto the vector
		allProcesses.push_back(process);

	}

	// Close basedir
	closedir(baseDirectory);

	// Return the vector or processes
	return allProcesses;
}
