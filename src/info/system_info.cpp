#include "system_info.h"
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>

using namespace std;


double get_uptime() {
	
	// Open file
	ifstream timeFile(PROC_ROOT "/uptime");

	// Check for error, If error,  print error and exit
	if (!timeFile) {
		cerr << "Unable to read from /proc/uptime" << endl;
		exit(EXIT_FAILURE);
	}


	double uptime;
	timeFile >> uptime;
  return uptime;
}


SystemInfo get_system_info() {
  	
	// Gather data
	SystemInfo sysFo;
	vector<ProcessInfo> procFo = get_all_processes(PROC_ROOT);
	
	//git number of processes
	sysFo.num_processes = procFo.size();

	// Get num of threads
	int numThreads = 0;
	int numUsThreads = 0;
	int numKerThreads = 0;
	int numRun = 0;		// Number of running states
	for (unsigned int i = 0; i < procFo.size(); i++) {
		//numThreads = numThreads + procFo[i].num_threads;
		if(procFo[i].state == 'R') {
			numRun++;		// If running state ++
		}
	
		if (procFo[i].is_kernel_thread()){
			numKerThreads++;
		}
		for (unsigned int j = 0; j < procFo[i].threads.size(); j++) {
			if (procFo[i].threads[j].is_user_thread()){
				numUsThreads++;
			} 
			if (procFo[i].threads[j].is_thread()){
				numThreads++;
			}
		}
	} 	
	sysFo.num_threads = numThreads;
	sysFo.num_user_threads = numUsThreads;
	sysFo.num_kernel_threads = numKerThreads;
	sysFo.num_running = numRun;
	sysFo.cpus = get_cpu_info();
//	double ups = 0;
//	for (unsigned int i = 1; i < sysFo.cpus.size(); i++){
//		if (sysFo.cpus[i].total_time() > ups) {
//		ups = sysFo.cpus[i].total_time();
//		}
//	}		
//	sysFo.uptime = ups/100; //sysFo.cpus[0].total_time() / 800.0;
	sysFo.uptime = get_uptime(); //sysFo.cpus[0].total_time() /  8.0  / CLOCKS_PER_SEC;
	sysFo.processes = procFo;
	sysFo.memory_info = get_memory_info();
	sysFo.load_average = get_load_average();



  return sysFo;
}
