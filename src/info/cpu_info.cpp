#include "cpu_info.h"
#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;


vector<CpuInfo> get_cpu_info() {
  	
	// Open file
	ifstream cpuFile(PROC_ROOT "/stat");

	// Check for error, IF error print and exit
	if (!cpuFile) {
		cerr << "Unable to read from /proc/meminfo" << endl;
		exit(EXIT_FAILURE);
	}

	// Declare a vector of cpuInfo's
	vector<CpuInfo> allCpuInfo;	// Vector of cpu's

	// Search through the file and populate stuff
	bool fishes = true;
	
	while (fishes) {
		CpuInfo cpuInfo;	// Yes, I did that. But case sensitive so I don't care. Ha.
		string sharkAte;
		cpuFile >> sharkAte;
		sharkAte = sharkAte.substr(0,3);
		if (sharkAte == "cpu") {
			// Populate the cpuInfo
			cpuFile	>> cpuInfo.user_time
				>> cpuInfo.nice_time
				>> cpuInfo.system_time
				>> cpuInfo.idle_time
				>> cpuInfo.io_wait_time
				>> cpuInfo.irq_time
				>> cpuInfo.softirq_time
				>> cpuInfo.steal_time
				>> cpuInfo.guest_time
				>> cpuInfo.guest_nice_time;
			// Push the cpuInfo onto the vector
			allCpuInfo.push_back(cpuInfo); 
		} else {
			fishes = false;			// No more cpu's to eat, break out of while loop
		}
	}
	return allCpuInfo;
}


CpuInfo operator -(const CpuInfo& lhs, const CpuInfo& rhs) {
  	
	// Create a cpuinfo var
	CpuInfo cpuInfo;		// Because that isn't confusing
	
	// Do the subtraction, pretty self explanitory
	cpuInfo.user_time = lhs.user_time - rhs.user_time;
	cpuInfo.nice_time = lhs.nice_time - rhs.nice_time;
	cpuInfo.system_time = lhs.system_time - rhs.system_time;
	cpuInfo.idle_time = lhs.idle_time - rhs.idle_time;
	cpuInfo.io_wait_time = lhs.io_wait_time - rhs.io_wait_time;
	cpuInfo.irq_time = lhs.irq_time - rhs.irq_time;
	cpuInfo.softirq_time = lhs.softirq_time - rhs.softirq_time;
	cpuInfo.steal_time = lhs.steal_time - rhs.steal_time;
	cpuInfo.guest_time = lhs.guest_time - rhs.guest_time;
	cpuInfo.guest_nice_time = lhs.guest_nice_time - rhs.guest_nice_time;
	
	// Return cpuInfo
	return cpuInfo;
}
