/**
 * Contains the main() routine of what will eventually be your version of top.
 */

#include <cstdlib>
#include <ncurses.h>
#include <iostream>
#include "./info/system_info.h"
#include "./utils/stats.h"
#include "./utils/flags.h"
#include <stdlib.h>
using namespace std;

void showHelp();

/**
 * Gets a character from the user, waiting for however many milliseconds that
 * were passed to timeout() below. If the letter entered is q, this method will
 * exit the program.
 */
void exit_if_user_presses_q() {
  char c = getch();

  if (c == 'q') {
    endwin();
    exit(EXIT_SUCCESS);
  }

  if (c == 'h') {
	endwin();
	showHelp();
	initscr();

}
}


void showHelp() {
	cout 	<< endl << "**********************************************" << endl << endl;
	cout  	<< "WELCOME TO THE HELP SECTION" << endl;
	
	cout  	<< "mytop supports the following command line flags: " << endl << endl
		<< "-d --delay=DELAY" << endl
		<< "Delay between updates, in tenths of seconds" << endl << endl
		<< "-s --sort-key COLUMN" << endl
		<< "Sort by this column; one of: PID, CPU, MEM, or TIME" << endl << endl
		<< "-h --help" << endl
		<< "Display a help message about these flags and exit" << endl << endl;

	cout 	<< "To exit or quit mytops, press 'q'" << endl << endl;
	
	cout 	<< "Press anykey then enter to continue to mytop: " << endl;
	string wait;
	cin >> wait;	

	cout << endl << "****************************************************" << endl;

}

/**
 * Entry point for the program.
 */
int main(int argc, char **argv) {

	// Get inputs
	vector<string> inputs = getFlag(argc, argv);
	unsigned userDelay = atoi(inputs[0].c_str());
	// Determine inputs,
	if (userDelay == 0) {
		userDelay = 1000;
	} else {
		userDelay = userDelay*100;
	}	
	if (inputs[2] != "0") {
		showHelp();
	}

	//Start doing mytop stuff
	// ncurses initialization
	initscr();

	// Don't show a cursor.
	curs_set(FALSE);

	// Set getch to return after 1000 milliseconds; this allows the program to
	// immediately respond to user input while not blocking indefinitely.
	timeout(userDelay);

//	int tick = 1;

	SystemInfo lastSys = get_system_info();
	SystemInfo newSys = get_system_info();

	while (true) {
		
		// Update SystemInfo
		lastSys = newSys;
		newSys = get_system_info();

		// Get Top Statitisiticsitsiss...However you spell that
		topStats display = get_topStats(lastSys, newSys, inputs[1]);
		wclear(stdscr);

		// Display the counter using printw (an ncurses function)
		//printw("Behold, the number:\n%d", tick++);


		// Print top stats
		printw("Welcome to my top!!! \n");
		printw("System uptime:\t%.3f  %s \n",
				display.sysUpTime, display.sysUpTimeUnit.c_str());
		printw("Average sytem load: \t%.2f% Over 1 minute, \t%.2f% Over 5 minutes, \t%.2f% Over 15 minutes\n",
						display.loadAvgOne, display.loadAvgFive, display.loadAvgFifteen);
		printw("Memory: \t%.3f %s Total, \t%.3f %s in Use, \t%.3f %s Free\n", 
				display.totalMemory, display.totalMemUnit.c_str(), 
				display.memInUse, display.memInUseUnit.c_str(),
				display.memAvail, display.memAvailUnit.c_str());
	
		printw("\nCPU Utilization: \tProcessor \tUser Mode \tKernel Mode \tIdling\n"); 			
		for (unsigned i = 0; i < display.cpuUtilize.size(); i++) {
			// Print Processor number
			if (i == 0) {
				printw("\t\t\tOverall");
			} else {              		
				printw("\t\t\t%u" , i);
			}
			printw("\t\t%.2f% \t\t%.2f% \t\t%.2f% \n",
				 display.cpuUtilize[i].userCent, display.cpuUtilize[i].kernelCent, display.cpuUtilize[i].idleCent);
		}

		printw("\nProcesses: \t%u total, \t%u running, \t%u threads", 
					display.totalProcExist, display.totalProcRun, display.totalThreads);
    	
		printw("\n\tPID \tCPU% \tState \tRSS \t\tTime+ \t\t\tCommand\n");
		
		for (unsigned i=0; i < 10; i++) {
			printw("\t%i \t%.2f\t%c \t%.3f %s \t%.2f\t%s   \t%s \n",
				display.sortProc[i].pid, 
				display.sortProc[i].cpuPercent,
				display.sortProc[i].currentState,
				display.sortProc[i].resMem, display.sortProc[i].resMemUnit.c_str(),
				display.sortProc[i].procTime, display.sortProc[i].procTimeUnit.c_str(),
				display.sortProc[i].theCmdLine.c_str());
		}
		
		
		
		// Redraw the screen.
    		refresh();

    		// End the loop and exit if Q is pressed
    		exit_if_user_presses_q();
  	}	

  	// ncurses teardown
  	endwin();



	return EXIT_SUCCESS; 
}
